# -*- coding: utf-8 -*-
import csv
from collections import defaultdict


def kosaraju(graph):
    # первый проход
    visited = set()
    stack = []

    def dfs1(vertex):
        visited.add(vertex)
        for neighbor in graph[vertex]:
            if neighbor not in visited:
                dfs1(neighbor)
        stack.append(vertex)

    for vertex in graph:
        if vertex not in visited:
            dfs1(vertex)

    # второй проход
    visited.clear()
    components = []

    def dfs2(vertex):
        visited.add(vertex)
        component.append(vertex)
        for neighbor in transposed[vertex]:
            if neighbor not in visited:
                dfs2(neighbor)

    transposed = {v: set() for v in graph}
    for vertex in graph:
        for neighbor in graph[vertex]:
            transposed[neighbor].add(vertex)

    while stack:
        vertex = stack.pop()
        if vertex not in visited:
            component = []
            dfs2(vertex)
            components.append(component)

    return components


if __name__ == '__main__':
    # тащим граф из файла
    # with open('out.csv', 'r') as csv_file:
    #     csv_reader = csv.reader(csv_file, delimiter=',', skipinitialspace=True)
    #     new_graph = {int(row[0]): [] if row[1] == '' else [int(i) for i in row[1:]] for row in csv_reader}

    test_graph = {
        1: [4],
        2: [1],
        3: [2],
        4: [3, 6, 5],
        5: [6],
        6: [7],
        7: [5]
    }

    largest = kosaraju(test_graph)
    print("Максимальная сильно связанная компонента:", largest)

    with open('largest.csv', 'w') as out_file:
        for arr in largest:
            out_file.write(' '.join(str(i) for i in arr))
            out_file.write('\n')
