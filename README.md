# Repository to store graph tasks

### Start guide
```
cd *Directory containing this file*
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
python3 random_graph.py
python3 dfs&bfs.py
```

### Testing
```
python -m unittest test_create_graph.py 
```