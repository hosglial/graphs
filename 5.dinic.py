from collections import deque


class Edge:
    def __init__(self, v, w, capacity, reverse):
        self.v = v
        self.w = w
        self.capacity = capacity
        self.reverse = reverse


class FlowNetwork:
    def __init__(self, V):
        self.V = V
        self.adj = [[] for _ in range(V)]

    def add_edge(self, v, w, capacity):
        forward = Edge(v, w, capacity, None)
        reverse = Edge(w, v, 0, forward)
        forward.reverse = reverse
        self.adj[v].append(forward)
        self.adj[w].append(reverse)

    def bfs(self, s, t):
        self.level = [-1] * self.V
        self.level[s] = 0
        q = deque()
        q.append(s)
        while q:
            v = q.popleft()
            for e in self.adj[v]:
                if self.level[e.w] < 0 and e.capacity > 0:
                    self.level[e.w] = self.level[v] + 1
                    q.append(e.w)
        return self.level[t] >= 0

    def dfs(self, v, t, f):
        if v == t:
            return f
        for e in self.adj[v]:
            if e.capacity > 0 and self.level[e.w] > self.level[v]:
                d = self.dfs(e.w, t, min(f, e.capacity))
                if d > 0:
                    e.capacity -= d
                    e.reverse.capacity += d
                    return d
        return 0

    def max_flow(self, s, t):
        flow = 0
        while self.bfs(s, t):
            while True:
                f = self.dfs(s, t, float('inf'))
                if f == 0:
                    break
                flow += f
        return flow


# пример использования
g = FlowNetwork(6)
g.add_edge(0, 1, 16)
g.add_edge(0, 2, 13)
g.add_edge(1, 2, 10)
g.add_edge(2, 1, 4)
g.add_edge(1, 3, 12)
g.add_edge(3, 2, 9)
g.add_edge(2, 4, 14)
g.add_edge(4, 3, 7)
g.add_edge(3, 5, 20)
g.add_edge(4, 5, 4)
print(g.max_flow(0, 5))
