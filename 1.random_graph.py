import random
import configparser

try:
    import networkx as nx
except ModuleNotFoundError:
    pass
from matplotlib import pyplot as plt


def create_graph(nodes: int, links: int, direct: bool):
    nodes_list = list(range(nodes))

    node_links = {i: list(random.choices(nodes_list, k=links)) for i in range(nodes)}

    graph = {i: set() for i in range(nodes)}

    for i in range(nodes):
        for j in node_links[i]:
            if len(graph[i]) < links and direct and i not in graph[j] and i != j:
                graph[i].add(j)
            if not direct and len(graph[j]) < links and len(graph[i]) < links:
                graph[i].add(j)
                graph[j].add(i)

    return graph


if __name__ == '__main__':
    # читаем конфиг с количеством вершин и связей
    config = configparser.ConfigParser()
    config.read('setup.cfg')
    config = config['DEFAULT']

    # создаем граф
    new_graph = create_graph(
        config.getint('Nodes'),
        config.getint('Links'),
        config.getboolean('Direct')
    )

    # запись csv и одновременный вывод на печать
    with open('out.csv', 'w') as file:
        for key, val in new_graph.items():
            print(f'{key}: {", ".join(str(i) for i in val)}')
            file.write(f'{key}, {", ".join(str(i) for i in val)}\n')

    with open('alg.csv', 'w') as file:
        for key, arr in new_graph.items():
            weights = [random.randint(10, 100) for _ in range(len(arr))]
            for node, weight in zip(arr, weights):
                file.write(f'{key},{node},{weight}\n')

    # импортируем граф в networkx и рисуем через matplotlib
    try:
        if config.getboolean('Direct'):
            G = nx.DiGraph(new_graph)
        else:
            G = nx.Graph(new_graph)
        # Если граф слишком большое генерируется исключение
        # Ловим его и пишем ошибку
        nx.draw(
            G,
            with_labels=True,
            node_color='w',
            arrows=config.getboolean('Direct'),
            **{'arrowstyle': '-|>'} if config.getboolean('Direct') else {}
        )
        plt.show()
        pass
    except ModuleNotFoundError:
        print('Can not draw, graph too large')
    except NameError:
        print('Undefined name')
