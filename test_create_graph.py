# -*- coding: utf-8 -*-
import random
from unittest import TestCase

from tqdm import tqdm

from random_graph import create_graph


class TestCreateGraph(TestCase):

    def test_undirect_graph(self):
        for _ in tqdm(range(100)):
            nodes, links = random.randint(0, 1000), random.randint(0, 1000)
            result = create_graph(nodes, links, False)
            for first_node in result.keys():
                for second_node in result[first_node]:
                    assert first_node in result[second_node]
                    assert second_node in result[first_node]
            max_links = max([len(result[i]) for i in result.keys()], default=0)
            self.assertTrue(max_links <= links)

    def test_direct_graph(self):
        for _ in tqdm(range(100)):
            nodes, links = random.randint(0, 1000), random.randint(0, 1000)
            result = create_graph(nodes, links, True)
            max_links = max([len(result[i]) for i in result.keys()])
            self.assertTrue(max_links <= links)
