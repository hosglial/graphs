# -*- coding: utf-8 -*-
import random
import csv


def generate_bipartite_graph(num_vertices_left, num_vertices_right, edge_probability):
    """
    Генерация случайного двудольного графа.

    :param num_vertices_left: количество вершин в левой доле графа.
    :param num_vertices_right: количество вершин в правой доле графа.
    :param edge_probability: вероятность наличия ребра между вершинами.
    :return: список смежности двудольного графа.
    """
    left = [i for i in range(num_vertices_left)]
    l = len(left)
    right = [l + i for i in range(num_vertices_right)]

    print(left)
    print(right)

    adj_list = []

    for l in left:
        for r in right:
            if random.random() < edge_probability:
                adj_list.append((l, r))

    return adj_list


if __name__ == '__main__':
    graph = generate_bipartite_graph(1000, 1200, 0.2)
    with open('bipartie.csv', 'w') as file:
        file.write(f'left,right\n')
        for i in graph:
            file.write(f'{i[0]},{i[1]}\n')
