import csv
from collections import deque


def non_rec_dfs(gr: dict, node: str):
    """Нерекурсивная реализация поиска в глубину"""
    stack = deque()
    visited = []
    stack.append(node)

    while stack:
        current_node = stack.pop()
        visited.append(current_node)
        for neib in gr[current_node]:
            if neib not in visited and neib not in stack:
                stack.append(neib)

    print('Поиск в глубину')
    print(visited)
    print(f'Граф {"связный" if len(visited) == len(gr.keys()) else "несвязный"}\n')


def non_rec_bfs(gr: dict, node: str):
    """Нерекурсивная реализация поиска в ширину"""
    queue = deque()
    visited = []
    queue.append(node)
    visited.append(node)

    while queue:
        current_node = queue.pop()

        for neib in gr[current_node]:
            if neib not in visited:
                queue.append(neib)
                visited.append(neib)

    print('Поиск в ширину')
    print(visited)
    print(f'Граф {"связный" if len(visited) == len(gr.keys()) else "несвязный"}\n')


if __name__ == '__main__':
    # тащим граф из файла
    with open('out.csv', 'r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',', skipinitialspace=True)
        new_graph = {row[0]: [] if row[1] == '' else row[1:] for row in csv_reader}

    # выполняем поиск в ширину и глубину
    non_rec_dfs(new_graph, '0')
    non_rec_bfs(new_graph, '0')
