import csv
import numpy as np
from scipy.optimize import linear_sum_assignment

# Load data from CSV file
with open('bipartie.csv', mode='r') as graph_file:
    reader = csv.DictReader(graph_file)
    edges = [(int(row['left']), int(row['right'])) for row in reader]

# Convert edges into adjacency matrix
max_left = max(edge[0] for edge in edges)
max_right = max(edge[1] for edge in edges)
adj_matrix = np.zeros((max_left, max_right))
for edge in edges:
    adj_matrix[edge[0]-1, edge[1]-max_left-1] = 1

# Run Hungarian algorithm
row_idx, col_idx = linear_sum_assignment(1 - adj_matrix)

# Print results
total_weight = adj_matrix[row_idx, col_idx].sum()
matches = [(i+1, j+max_left+1) for i, j in zip(row_idx, col_idx)]
print(f"Matching: {matches}")
print(f"Total weight: {total_weight}")