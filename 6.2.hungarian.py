import csv

from tqdm import tqdm


class BipartiteGraph:
    def __init__(self, n):
        self.graph = [[] for _ in range(n)]
        self.visited = [False] * n
        self.match = [-1] * n

    def add_edge(self, u, v):
        self.graph[u].append(v)

    def dfs(self, u):
        stack = [u]
        while stack:
            u = stack.pop()
            for v in self.graph[u]:
                if not self.visited[v]:
                    self.visited[v] = True
                    if self.match[v] == -1:
                        self.match[v] = u
                        return True
                    else:
                        stack.append(self.match[v])
                        self.match[v] = u
        return False

    def max_matching(self):
        count = 0
        for i in tqdm(range(len(self.graph))):
            self.visited = [False] * len(self.graph)
            if self.dfs(i):
                count += 1
        return count


with open('bipartie.csv', 'r') as file:
    reader = csv.reader(file)
    edges = []
    for line in reader:
        edges.append((int(line[0]), int(line[1])))

    graph = BipartiteGraph(len(edges))

    for edge in edges:
        graph.add_edge(edge[0], edge[1])

# Вычисляем максимальное паросочетание
matching = graph.max_matching()

# Выводим результат
print("Максимальное паросочетание: ", matching)
