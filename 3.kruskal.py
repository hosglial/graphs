# -*- coding: utf-8 -*-
import csv

source_list = []
target_list = []
weight_list = []

with open('alg.csv', newline='') as f:
    reader = csv.DictReader(f)
    for row in reader:
        # print(row['source'], row['target'])
        source_list += [row['source']]
        target_list += [row['target']]
        weight_list += [row['weight']]


class Graph:
    def __init__(self, vertices):
        self.V = vertices
        self.graph = []

    def add_edge(self, u, v, w):
        self.graph.append([u, v, w])

    # Search function

    def find(self, parent, i):
        if parent[i] == i:
            return i
        return self.find(parent, parent[i])

    def apply_union(self, parent, rank, x, y):
        xroot = self.find(parent, x)
        yroot = self.find(parent, y)
        if rank[xroot] < rank[yroot]:
            parent[xroot] = yroot
        elif rank[xroot] > rank[yroot]:
            parent[yroot] = xroot
        else:
            parent[yroot] = xroot
            rank[xroot] += 1

    #  Applying Kruskal algorithm
    def kruskal_algo(self):
        result = []
        i, e = 0, 0
        self.graph = sorted(self.graph, key=lambda item: item[2])
        parent = []
        rank = []
        for node in range(self.V):
            parent.append(node)
            rank.append(0)
        while e < self.V - 1:
            u, v, w = self.graph[i]
            i = i + 1
            x = self.find(parent, u)
            y = self.find(parent, v)
            if x != y:
                e = e + 1
                result.append([u, v, w])
                self.apply_union(parent, rank, x, y)
        for u, v, weight in result:
            print("%d - %d: %d" % (u, v, weight))

        with open('kruskal.csv', 'w') as file:
            for u, v, weight in result:
                file.write("%d,%d,%d\n" % (u, v, weight))


if __name__ == '__main__':
    a = max(source_list)
    b = max(target_list)
    if a > b:
        d = a
    else:
        d = b

    num_edges = int(d) + 1
    g = Graph(num_edges)
    for num in range(len(source_list)):
        g.add_edge(int(source_list[num]), int(target_list[num]), int(weight_list[num]))

    g.kruskal_algo()
